2018.10.29

# Colonial Encounters: New England Puritans

- huge impact on later developments in literature
- religious, theocratic community
- "translated" to non-religious concepts
- protestant reform movement
- name "puritans" given to them by their critics (return to true faith, fundamentalists)
- new testament as model
- utopian community, never done before and impossible in Europe
- perfection as their demise

History begins in 16 century in England, reformation period. The puritan movement tried to reform church of England, shifted their focus on building the "perfect" christian **utopian community** by themselves **without interest in missionary work**.

There were struggles from the beginning due to the pursuit of perfection.

Families were migrating and planned to settle in America and turn their backs to Europe.

Plymouth Colony, founded 1620 by a group of settlers who arrived with the ship Mayflower. **Mythologized** as the pilgrim fathers (commemorated in the ritual of thanksgiving -> American national identity).  
William Bradford

Other settlement: Massachusetts Bay, 1630  
Arbella; Jonn Winthrop  
Grew very rapidly.

These colonies were organized as theocracies.

Conflicts with native Americans due to farming.

## John Winthrop, "A model of Christian Charity"

Sermon: Illustrates the centrality of religion in puritan culture.

Non-religious forms of culture (entertainment) were considered sinful and banned.

- religious, functional, sober
- Banning anything with drama and comedy
- "doctrine of total depravity"
- human nature: depraved, sinful

Doctrine started in education (ABC aphorisms, "New England Primer")

**Bible** was the most important "intertext" (**frame of reference**) for puritan writers (references to the bible → **literal word of god**). **Literacy was important** since it was thought that the Bible "contained" god.

Immediately established a public school system (revolutionary at the time, very high illiteracy numbers in Europe). Almost universal literacy was achieved. A **culture of reading and writing** was encouraged. That's why it had so much impact on later literature.

Language in the sermon is suprising:   
**language of contract law** to talk about the principles of the community and the relationship to god (contractional, covenantial relationship).

> "We have entered into convenant with Him [...]"  
*covenant*: contract; *Him*: god

**Catholic**: *covenant of works*: God expects a certain work from his people as a price of salvation (salvation can be earned: certain prayers, paying indulgances)

**Covenant of grace**: *predestination → impact on Puritan life-writing (self-examination)*  
Salvation is not something that can be earned, it's a matter of predestination. Preparation to receive the gift of salvation.  

Self-examination: *"Are there signs for whether I will be saved or not? What am I? What will my afterlife will be like?"*

> *"We must consider that we shall be as a city upon a hill. The eyes of all people are upon us"* (216)

- Beacon for the entire world
- Earliest formulation of what would later become a central ideology (discourse on **American exceptionalism**: America is unique and special among all cultures (better, superior) and has a role in the world)
- references to the bible (*"city upon a hill"*): Compares migrants to biblical people of Israel
    - America - 'New Israel', 'promised land'
    - **Analogy** between migrants and biblical people and America and the *promised land*

Used the bible to give meaning to their own experiences: Reflects the puritan way of making sense of the world around them.

Common practice in puritan culture: Typological way of making sense of experiences, people, etc. (looking for parallels to the bible) to decode God's messages to them.

Fundamentally metaphorical perspective on the world: worldly events, experiences as god's way of communicating with them.

→ *jeremiad*

*Voice of warning*: Difficulties, how hard to build the perfect christian community, horrible things that might happen when they don't do their job properly. (meant to be scary)

- *"prophesy of doom"* (Sacvan Bercovitch, The American Jeremiad (1978))
- lamenting current or potential failure to renew commitment (rhethorical goal: urge audience to hold onto ideals of the community)


> Other sorts of writings: religious on the surface but different contents

## Mary Rowlandson: "A Narrative of the Capitivity and Restoration of Mrs. Mary Rowlandson" (1682)

Mary Rowlandson: different glimpse because 

- she is not one of the leaders
- a woman
- the text is of a different genre: captivity narrative (immediate precursor of adventure literature)

Many genres still adhere to captivity narratives. Particular historical conflict: King Philip's War (1675-78) caused by puritan emproachments(?) on native lands.

Captivity by natives: Release against ransom.

Projects: Religious values, assurance of purity to re-enter the puritan community, propaganda: horrors of captivity

Heavily edited by husband and politician: Impossible to determine what the original voice was.

Buried under religious layer: Narrative of extreme experience, fear of death, violence, mundane challenges of living in native tribe

Religious narrative: as god's test of her faith in order to renew her commitment and prepare for gift of salvation.  
Natives → agents of the devil  
'wilderness' → *"lively resemblance of hell"*

Natives aren't actants, they're instruments to convey god's code

Subtext: changing perspective on natives and beginning to adapt to the culture (*acculturate*)

> *"[...] yet they were sweet and savoury to my taste."* (317)

Trading partners, customers: knitting/sewing services for food

> *"Another asked me to knit a pair of socks, for which she gave me a quart of peas."* (319)

No session next week.