2018-10-22

# Survey of American Literature

## Paradigms

_Where and how and when does American literature begin?_ -> Very intense debates

Before canon debates, the answer was 17th century in New England. This assumption ignores other languages.

_American civilization is a transplanted European civilization._

Colonial encounters (different constellations of contact with environment and people) is where American Literature begins (born out of various experiences of contact).

Writing was used as a political instrument: Claim ownership of land/continent, declaring ways of living superior to others, ...

Multiplicity is at the heart of the beginnings of American literature: multilingual, multicultural, multinational

Literature before encounters: Indigenous literature

## Native American Voices?

Natives play a role as images/objects in writing. Depiction of natives reflected European values.

Native literature looked different.  
European: Literature is tied to writing. (Cultures of writing and print)  
Native: Oral cultures ("orature" as equivalence to western institutions of literature)
*Existed before colonization and continues to exist*

Narratives, poetry and songs performed orally in indigenous communities, always involve more than just words (physical movements of the performer and the audience, ritualistic actions in which performances are embedded).

Looking at reduced versions (transcriptions/translation in terms of language and medium).

Prevalent text-types and genres: 

- historical narratives (few of which have been preserved)
- hero tales (important repository of tribes' values) 
- trickster tales (anti-hero narratives, cultural outsiders, usually failing, human weaknesses, depicts in a humorous way)
- origins and emergence stories (talk about a particular tribe's ideas about the origin of the world, cultural customs)

## "The Origin of Stories" (Seneca Nation)

Talks about the beginnings of storytelling. Fundamentally different from prevailing western literature.

Story is a gift of nature/earth, related by the story teller. Window to something that is beyond human understanding. Tied to a ritual: Giving gifts, saying words. Stories are a value, to be compensated/paid.

Important to realize: Limits to how much we can understand

## Euro-American Literatures of Colonial Encounter

Better preserved than indigenous literature

The idea of fiction is pretty young

Three major text types:

- travel reports (accounts of expeditions, geography, plants, animals, "indian" life and customs, prevalent, dominant in early phase of exploration and conquest)
- histories (accounts of the development of particular settlements, sometimes about conflicts, "indian wars" of the late colonial period, later phases of colonies' development)
- journals (accounts of individual experiences of colonial encounters)

> examples from 1542 to 1630
>
> Alvar Núñez Cabeza de Vaca *Relation*
>
> de las Casas was Monk of the catholic church, sent to convert indigenous to christianity

## Journal of the first Voyage to America

Pioneers a particular way of writing

Part transcript of original, part summary, not an original text

Challenges:

- to present an unfamiliar place: imagery of earthly paradise
- make it understandable for Europeans: looking through European eyes

Journal targeted at Spanish Crown, goal: persuading the Crown to invest in further expeditions

commodifying perspective: no gold but other exploitable resources, pointing out values that might lay in colonizing the place

clear image of white superiority, casts Columbus and crew as 'naturally' superior

subtext: 

- not in control of communication, understands what he wants to understand
- resistance: communities were doing everything to make people leave, trying to scare Columbus and crew away (cannibals)



*What is beneath methodical surface of the text?*

