<div style="float:right">2018.11.12</div>

# American Englightenment and Early Republic

Crisis in Puritan colonies:

- Conflicts with Native Americans,

- Salem Witchcraft Trials in late 17th century: charged with witchcraft and executed (final straws that contributed to crisis)

Intellectual and political transformations in the **18th century**:

- "Enlightenment"
    - helped articulate ideas of national independence
- "Revolutionary War (1775-1783)
- Declaration of Independence (4th of Julz, 1776)
    - war to defend the declaration
    - founding of the USA

## American Enlightenment Literature: Ideas & Interests

Literature paved the ground for the conflict.

**Benjamin Franklin**, *Autobiography*

- *Typical enlightenment-man*
- wrote about the principles on which the USA would be founded
- influential and successful scientiest
- invented lightning rod


New **perspective on human nature**:

Puritan: human nature is sinful  
Enlightenment: **Goodness** of human nature, born with capacity for **self-improvement**, to do good.

Ability to know right from wrong, work on oneself to perfection

→ found in Franklin's Autobiography as overall plot (journey of self-improvement)

→ pioneers a particular plot of self-improvement/success-story that impacted American culture, *'from rags to riches'*

Effort to emphasize the achievements of his self-improvement

> "*I have been the more particular in this description of my jouney  
[...]  
and about a shilling in copper*"

Confidence in science / **scientific method**:

- Marks another major break from the Puritan thinking
- Universe governed by 'laws of nature' that can be understood, not messages from god
- Human mind is made in order to understand laws of nature: **reason** is the core of human nature
- Great optimism that human reason and systematic study would solve the mysteries of the world

Franklin used a systematically scientific method to improve himself:

- Franklin looks at use of scientific method of youngers self with a good deal of irony
- Time table
    - Morning questionm, prepare for day, work, ...
    - Coming up with a system to make best use of time
- Project to master all the important virtues: become fully virtuous
    - "*What are important virtues that make my life better?*"
    - List of virtues: Temperance, Silence, Order, Resolution
    - Step by step approach: check book for keeping track

Humans are seen to possess **'natural' rights**:  
Not something that a person is given by the state but is born with

**Social compact**:  
States exist in order to protect natural rights of individuals

→ natural independence

Declaration of Independence:
> "*We hold these truths to be self-evident,  
[...]  
to effect their Safety and Happiness.*"

Enlightenment thinking fuels utopian image of America; American exceptionalism

J. Hector St. John den Crèvecoeur: *Letters from an American Farmer*:

- Authorial mask: Farmer James (fictional character to raise question: *What is America/an American?*)
- Epistolary form: Fiction that it is a set of letters 

> "*Here are no aristocratical families  
[...]  
We are the most perfect society now existing in the world.*"

Strategy of comparison: Cast American in utopian light by comparing it to negative aspects of Europe (feudalism, social injustice, inequality)

→ America as a farmer's republic:

- simple life style
- possible to own land

> "*What is then the American  
[...]  
are melted into a new race of men.*"

- American as a re-born man; melting pot: European → American
- America is welcoming everyone
- Identifies which individuals are meltable: Europeans
- larger Eurocentric bias in Enlightenment thinking → prevailing notion of reason (as manifesting itself in archievements of Western civilization)

Reason manifests itself in achievements of western civilization: **Evidence of reason**. 

Potential for empowerment and marginalization: Native Americans and African Americans are excluded.

