<span style="float:right;">2019-01-14</span>
# Literature & Culture I

## Modernism

- Modernism: beginning WW1 
- radically different society
- not held together by tradition
- liberation, promise, freedom
- fear, anxiety, disorientation
- → find ways to work through these experiences, divorced from the past
- tools and techniques
    - defamiliarization
    - experimentation at heart
    - new ways of expressing themselves
    - Ernest Heminway: minimalism

### Gertrude Stein

- interest in literature, visual arts, performing arts
- fascinated by cubism 
- challenged conventions:
    - fundamental conventions of language
    - referentiality
    - transparency
    
Most experimental example: "*Susie Asado*" (1913) → flamenco


- word stress to mark rhythm
- s/t sounds

## Modernism II: The Harlem/New Negro Renaissance

- "*Negro*" term of pride, no racist connotation at that time

- distinct history of urbanization and 'modernization'

- The 'Great Migration'
- new institution os fracism in post-Reconstruction South:
    - disenfranchisement
    - segretation ('Jim Crow laws')
- distinctly African-American experiences of modernity: hopes and frustrations
- → new Black communities: Harlem (shining emblem of new black communities, hope that was attached to them)

exc: Alain Locke, "*The new Negro*":  
"*Here in Manhattan [...] a race capital*"

- popularity of urban African-American culture; the 'jazz age'; white patronage

→ disrepancy between promises of modernity and lived experience of African-Americans

### W.E.B. Du Bois

- "double consciousness":

quote: "the Negro is a sort of seventh [...] from being torn asunder" (Souls of Black Folk)

### Langston Hughes

- "The Negro Speaks of Rivers"
- Inspired by free verse catalog
- looking for poetic models in venacular African-American culture: blues music → blues as iconic art-form of Harlem Renaissance

-

- personal narrative of frustration and discontent
- repitition (AAB); call and response
- syncopated rhythm

"The Weary Blues" (1923)

- setting: blues club
- poetic eye listening to blues performance in that club, expressing the experience of listening
- moved by performance
- content: blues as metonym of discontent and despair
- incorporating blues lyrics into poem

### Claude McKay

- immigrant from Jamaica
- appropriating classic European/Euro-American models of poetry

poem: "America" (1921)

- sonnet (three quatrains + one couplet; rhyme; iambic pentameter)