<div style="float: right;">2018.11.19</div>
# American Enlightenment Literature

## Early Republic

**Rise of imaginative writing and aesthetic forms of writing → beginning of the American novel**

There are forms of writing that are different from literature (political writings, etc.)

Literature that does not just speak to the mind but to the whole being of the reader: Emotional response (feeling, sensation, ...)

E.g. music: "not rationally interesting"

- New purpose: provide an aesthetic experience
- *novel* → *new* kinds of writings

---

Revolutionary style of writing that was also **scepticised**:

- Fastest growing segment of readers: women  
"What would happen to the minds of these women?"

Addressed scepticism by adding long prephases(?) that explained that the novel was actually grounded in fact, brought usefulness, etc.

e.g. Susanna Rowson, *Charlotte, A tale of Truth* (1791):

- "text could be real"
- "text is providing a lesson"

---

**Poetry**: Form of writing where language is controlled (by patterns that existed before)

Ideal: *well-made poem* that masters pre-existing forms

**Phillis Wheatley**

- First known publication by an African American writer  
- *Poems of Various Subjects, Religious and Moral*
- topics: elegies, 'heroic couplets'

> "On Being Brought from Africa to America" (1773):  
"*Twas mercy brought me [...] join th'angelic train.*"

- pro-slavery discourse: slavery 'civilizing' the 'savage', 'saved by whites'
- *lyrical I*: African and poetic
- religion: equality

## American Romanticism

Hudson River School: Interested in landscape paintings

- Thomas Cole, "The Oxbow" (1836)
- Albert Bierstadt, "Among the Sierra Nevada Mountains" (1868)
- Asher Brown Durand, "Kindred Spirits" (1849)

Reason and intellect not as important compared to Enlightenment period. Interested in anything that is irrational (which is what is making the human *human*). Capacity for intuition and imagination.

19th century: Rapid development in fiction:

- Short story was invented
- writing that advertises the power of imagination
- interest in (unique) individual; individual vs. society
- interest in nature (glorifying, spiritual experiences)
- interested in the past; creating a 'usable past': few have a contemporary setting (history, cultural origins)
- interest in history and legend

---

**Washington Irving, "Rip Van Winkle" (1819/20)**

- ironical play with fictionality: frame story based on mysterious manuscript → used to claim usefulness

> "*There have been various opinions [...], as a book of unquestionable authority*" (1248-49)

- link between America's present and past
- highlights that the story is not original but uses a European legend (Kyffhäuser Mountain)

> "*The foregoing Tale, [...], narrated with his usual fidelity*" (1259)

- protagonist: Rip Van Winkle
- fantastic plot: time travel, magic potions

> "*an obedient hen-pecked husband [...] Rip Van Winkle was thrice blessed*" (1250)

---

Figure of the **'noble savage'** (Rousseau); myth of the 'Vanishing Indian'

- imagined as closer to nature as 'civilized' European/American
- indian removal of 1830s, trail of tears: thousands of indigenous people forced to leave their homelands
- figure of 'noble savage' became adapted to myth of the vanishing indian (dying out), romantic figure

---

**James Fenimore Cooper**, *The Last of the Mohicans* (1826)

- Frontier romance
- colonial past: French & Indian War (mid-18th century)
- adapts European model: Sir Walter Scott's historical romances (conflicts between English and Scottish)
- "plot of the novel is extremely uninteresting and poorly written"
- frontier as mythical landscape
- frontier hero: self-reliant character, turned back to civilization, chose to live in the freedom of the frontier, mediates civilization and wilderness

> "*'I am not a prejudiced man [...], not feel a pride in striving to outdo them'*"
